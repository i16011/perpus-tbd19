<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class SPReport extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        
        $db->exec($this->SP_List_Denda());
    }

    private function SP_List_Denda(){
        return "
            DROP PROCEDURE IF EXISTS List_Denda;
            CREATE PROCEDURE List_Denda(
                IN query varchar(256))
                proc_Exit:BEGIN
                    SELECT user.id , SUM(Calculate_Denda(meminjam.id)) 
                    FROM meminjam 
                    INNER JOIN user ON meminjam.fkUser = user.id 
                    GROUP BY user.id
                    ORDER BY SUM(Calculate_Denda(meminjam.id)) DESC;
                END;
        ";
    }

    public function on_failed(\Exception $e) {

    }
}
