<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class UpdateTable extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $db->exec($this->Update_Table_Buku());
        $db->exec($this->SP_Insert_Buku());
        $db->exec($this->SP_Insert_Denda());
        $db->exec($this->SP_Insert_Denda_Rule());
    }

    public function on_failed(\Exception $e) {

    }

    private function Update_Table_Buku(){
        return "ALTER TABLE Buku
        DROP COLUMN fkTag;
        ";
    }

    private function SP_Insert_Buku(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Buku;
            CREATE PROCEDURE Insert_Buku(
                IN fk_penerbit INT,
                IN fk_tag INT,
                IN fk_user INT,
                IN fk_pengarang INT,
                IN name varchar(256),
                IN published_on datetime,
                IN created_on datetime,
                OUT last_inserted_id INT)
                proc_Exit: BEGIN
                    DECLARE id_penerbit INT DEFAULT -1;
                    DECLARE id_tag INT DEFAULT -1;
                    DECLARE id_user INT DEFAULT -1;
                    DECLARE id_pengarang INT DEFAULT -1;

                    SELECT
                        id INTO id_penerbit
                    FROM
                        Penerbit
                    Where
                        Penerbit.id = fk_penerbit;
                    
                    if id_penerbit = -1 THEN
                        -- error karena tidak ada id penerbit
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Penerbit id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_pengarang
                    FROM
                        Pengarang
                    Where
                        Pengarang.id = fk_pengarang;
                    
                    if id_pengarang = -1 THEN
                        -- error karena tidak ada id penerbit
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Pengarang id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_tag
                    FROM
                        Tag
                    Where
                        Tag.id = fk_tag;
                    
                    if id_tag = -1 THEN
                        -- error karena tidak ada id tag
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Tag id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_user
                    FROM
                        User
                    Where
                        User.id = fk_user;
                    
                    if id_user = -1 THEN
                        -- error karena tidak ada id user
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'User id not found';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Buku (fkPenerbit, fkUser, name, published_on, created_on, deleted_on)
                    VALUES (fk_penerbit, fk_user, name, published_on, created_on, null);

                    SET last_inserted_id = LAST_INSERT_ID();

                    INSERT INTO BukuPengarang (fkPengarang, fkBuku)
                    VALUES (id_pengarang, last_inserted_id);

                    INSERT INTO TagBuku (fkTag, fkBuku)
                    VALUES (id_tag, last_inserted_id);

                    INSERT INTO Eksemplar (fkJenis, fkBuku, total)
                        SELECT EksemplarJenis.id, last_inserted_id, 0 
                        FROM EksemplarJenis;

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Denda(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Denda;
            CREATE PROCEDURE Insert_Denda(
                IN fk_jenis INT,
                IN not_before datetime,
                IN expired_on datetime,
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE denda_id INT DEFAULT -1;
                    DECLARE jenis_id INT DEFAULT -1;

                    SELECT
                        id INTO jenis_id
                    FROM
                        EksemplarJenis
                    Where
                        EksemplarJenis.id = fk_jenis;

                    if fk_jenis = -1 THEN
                        -- error karena tidak ada fk_jenisnya , sp selesai
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'fk_jenis not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO denda_id
                    FROM
                        Denda
                    WHERE
                        Denda.not_before < not_before AND Denda.expired_on > expired_on AND Denda.fkJenis = fk_jenis;

                    if denda_id != -1 THEN
                        -- error karena ada sudah ada sebuah denda pada waktu tersebut untuk jenis tersebut , sp selesai
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Sudah ada denda yang berlaku';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Denda (fkJenis, not_before, expired_on)
                    VALUES (fk_jenis, not_before, expired_on);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Denda_Rule(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Denda_Rule;
            CREATE PROCEDURE Insert_Denda_Rule(
                IN fk_denda INT,
                IN precedence INT,
                IN min_day INT,
                IN penalty_price decimal(13,2),
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE id_denda INT DEFAULT -1;
                    DECLARE id_denda_rule INT DEFAULT -1;

                    SELECT
                        id INTO id_denda
                    FROM
                        Denda
                    Where
                        Denda.id = fk_denda;

                    if id_denda = -1 THEN
                        -- error karena tidak ada id denda
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Denda id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_denda_rule
                    FROM
                        DendaRule
                    Where
                        DendaRule.precedence = precedence AND DendaRule.fkDenda = id_denda;

                    if id_denda_rule != -1 THEN
                        -- error karena sudah ada denda rule untuk denda tersebut dengan precedence parameter
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Sudah ada denda rule yang berlaku';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO DendaRule (fkDenda, precedence, min_day, penalty_price)
                    VALUES (fk_denda, precedence, min_day, penalty_price);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END
        ";
    }
}
