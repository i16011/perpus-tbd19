<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class SPStockBuku extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $db->exec($this->Function_Stock_Buku());
    }

    public function on_failed(\Exception $e) {

    }

    private function Function_Stock_Buku(){
        return "
        DROP FUNCTION IF EXISTS Stock_Buku;
        CREATE FUNCTION Stock_Buku (idEksemplar INT)
        RETURNS INT
        BEGIN
            DECLARE terpinjam INT DEFAULT 0;
            DECLARE stock INT DEFAULT 0;
            
            SELECT
            	Eksemplar.total INTO stock
            FROM
            	Eksemplar
            WHERE
            	Eksemplar.id = idEksemplar;
            
            SELECT
                COUNT(id) INTO terpinjam
            FROM
                Meminjam
            WHERE
                Meminjam.returned_on IS NULL AND Meminjam.fkEksemplar = idEksemplar;
                
            RETURN (stock-terpinjam);
        END
        ";
    }
}
