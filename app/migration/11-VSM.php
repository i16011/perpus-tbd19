<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class VSM extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $db->exec($this->SP_VSM_Update_All_Books());
        $db->exec($this->Function_Split_String());
        $db->exec($this->SP_Kata_To_Table());
    }

    public function on_failed(\Exception $e) {

    }

    private function SP_VSM_Update_All_Books(){
        return "
            DROP PROCEDURE IF EXISTS VSM_Update_All_Books;
            CREATE PROCEDURE VSM_Update_All_Books(
                )
                proc_Exit: BEGIN
                    DECLARE id_Buku INT DEFAULT -1;
                    DECLARE judul_Buku varchar(256) DEFAULT NULL;
                    DECLARE finished INT DEFAULT 0;

                    DECLARE cursor_buku CURSOR
                        FOR
                            SELECT
                                id, name
                            FROM
                                Buku;

                    -- fetch status
                    DECLARE CONTINUE HANDLER 
                    FOR NOT FOUND SET finished = 1;

                    OPEN cursor_buku;

                    FETCH cursor_buku
                    INTO id_Buku, judul_Buku;

                    WHILE finished = 0 DO
                        CALL vsm_update_words_book(id_Buku, judul_Buku);

                        FETCH cursor_buku
                        INTO id_Buku, judul_Buku;
                    END WHILE;

                    CLOSE cursor_buku;
                END;
        ";
    }

    private function Function_Split_String(){
        return "
            CREATE FUNCTION SPLIT_STR(
                x VARCHAR(255),
                delim VARCHAR(12),
                pos INT
            )
            RETURNS VARCHAR(255)
            RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
                    LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
                    delim, '');
        ";
    }

    private function SP_Kata_To_Table(){
        return "DROP PROCEDURE IF EXISTS kata_to_table;
        
        CREATE PROCEDURE kata_to_table(fullstr VARCHAR(255))
        
        BEGIN
            DECLARE a INT Default 0 ;
            DECLARE str VARCHAR(255);
        
            CREATE TEMPORARY TABLE my_temp_table(
                kata  VARCHAR(255),
                total int DEFAULT 0
            );
            
            simple_loop: LOOP
            SET a=a+1;
            SET str=SPLIT_STR(fullstr,' ',a);
            IF str='' THEN
                LEAVE simple_loop;
            END IF;
            #Do Inserts into temp table here with str going into the row
            SELECT @jumlah := count(kata) from my_temp_table
                where kata=str;
            IF(@jumlah >= 1) THEN
                UPDATE my_temp_table set total = total + 1 where kata = str;
            ELSE
                insert into my_temp_table values (str, 1);
            end if;
                    
            END LOOP simple_loop;
                    
                
            SELECT *
                FROM
                my_temp_table;
            
        END;";
    }
}
