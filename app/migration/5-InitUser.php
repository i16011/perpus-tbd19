<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class InitUser extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $res = $db->exec("CALL Insert_User(:fk_acl, :username, :password, :email, @kambing)", [
            ":fk_acl" => 1,
            ":username" => "admin",
            ":password" => \Model\User::hash_password("localhost"),
            ":email" => "admin@perpus",
        ]);

        var_dump($res);
    }

    public function on_failed(\Exception $e) {

    }
}
