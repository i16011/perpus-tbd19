<?php
namespace Migration;

/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class DBGeneration extends \Chez14\Ilgar\MigrationPacket
{
    public function on_migrate()
    {
        $f3 = \F3::instance();

        // Tabel blablabla
        $db = $f3->get('DB');
        $db->exec($this->drop_all_table());
        $db->exec($this->create_penerbit());
        $db->exec($this->create_tag());
        $db->exec($this->create_pengarang());
        $db->exec($this->create_eksemplarJenis());
        $db->exec($this->create_ACL());
        $db->exec($this->create_ACLItem());
        $db->exec($this->create_user());
        $db->exec($this->create_buku());
        $db->exec($this->create_eksemplar());
        $db->exec($this->create_denda());
        $db->exec($this->create_dendaRule());
        $db->exec($this->create_meminjam());
        $db->exec($this->create_pesanan());
        $db->exec($this->add_foreign_key());
        // taro SP disini
    }

    public function on_failed(\Exception $e)
    { }

    private function drop_all_table()
    {
        return "
        DROP TABLE IF EXISTS Pesanan;
        DROP TABLE IF EXISTS Meminjam;
        DROP TABLE IF EXISTS DendaRule;
        DROP TABLE IF EXISTS Denda;
        DROP TABLE IF EXISTS Eksemplar;
        DROP TABLE IF EXISTS Buku;
        DROP TABLE IF EXISTS User;
        DROP TABLE IF EXISTS ACLItem;
        DROP TABLE IF EXISTS ACL;
        DROP TABLE IF EXISTS EksemplarJenis;
        DROP TABLE IF EXISTS Pengarang;
        DROP TABLE IF EXISTS Tag;
        DROP TABLE IF EXISTS Penerbit;
        ";
    }

    private function add_foreign_key()
    {
        return "
            ALTER TABLE ACLItem ADD FOREIGN KEY (fkACL) REFERENCES ACL(id);

            ALTER TABLE User ADD FOREIGN KEY (fkACL) REFERENCES ACL(id);
            
            ALTER TABLE Buku ADD FOREIGN KEY (fkPenerbit) REFERENCES Penerbit(id);
            ALTER TABLE Buku ADD FOREIGN KEY (fkUser) REFERENCES User(id);
            
            ALTER TABLE Eksemplar ADD FOREIGN KEY (fkJenis) REFERENCES EksemplarJenis(id);
            ALTER TABLE Eksemplar ADD FOREIGN KEY (fkBuku) REFERENCES Buku(id);
            
            ALTER TABLE Denda ADD FOREIGN KEY (fkJenis) REFERENCES EksemplarJenis(id);
            
            ALTER TABLE DendaRule ADD FOREIGN KEY (fkDenda) REFERENCES Denda(id);
            
            ALTER TABLE Meminjam ADD FOREIGN KEY (fkEksemplar) REFERENCES Eksemplar(id);
            ALTER TABLE Meminjam ADD FOREIGN KEY (fkUser) REFERENCES User(id);
            
            ALTER TABLE Pesanan ADD FOREIGN KEY (fkEksemplar) REFERENCES Eksemplar(id);
            ALTER TABLE Pesanan ADD FOREIGN KEY (fkUser) REFERENCES User(id);
        ";
    }

    private function drop_foreign_key()
    {
        return "
            ALTER TABLE ACLItem DROP FOREIGN KEY (fkACL);

            ALTER TABLE User DROP FOREIGN KEY (fkACL);
            
            ALTER TABLE Buku DROP FOREIGN KEY (fkPenerbit);
            ALTER TABLE Buku DROP FOREIGN KEY (fkUser);
            
            ALTER TABLE Eksemplar DROP FOREIGN KEY (fkJenis);
            ALTER TABLE Eksemplar DROP FOREIGN KEY (fkBuku);
            
            ALTER TABLE Denda DROP FOREIGN KEY (fkJenis);
            
            ALTER TABLE DendaRule DROP FOREIGN KEY (fkDenda);
            
            ALTER TABLE Meminjam DROP FOREIGN KEY (fkEksemplar);
            ALTER TABLE Meminjam DROP FOREIGN KEY (fkUser);
            
            ALTER TABLE Pesanan DROP FOREIGN KEY (fkEksemplar);
            ALTER TABLE Pesanan DROP FOREIGN KEY (fkUser);
        ";
    }

    private function create_penerbit()
    {
        return "CREATE TABLE Penerbit (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name varchar(256)
        )
        ";
    }

    private function create_tag()
    {
        return "CREATE TABLE Tag(
            id INT AUTO_INCREMENT PRIMARY KEY,
            name varchar(256)
        )
        ";
    }

    private function create_pengarang()
    {
        return "CREATE TABLE Pengarang(
            id INT AUTO_INCREMENT PRIMARY KEY,
            name varchar(256)
        )
        ";
    }

    private function create_eksemplarJenis()
    {
        return "CREATE TABLE EksemplarJenis(
            id INT AUTO_INCREMENT PRIMARY KEY,
            name varchar(256)
        )
        ";
    }

    private function create_ACL()
    {
        return "CREATE TABLE ACL(
            id INT AUTO_INCREMENT PRIMARY KEY,
            name varchar(256)
        )
        ";
    }

    private function create_ACLItem()
    {
        return "CREATE TABLE ACLItem(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkACL INT,
            action_name varchar(256),
            value INT
        )
        ";
    }

    private function create_user()
    {
        return "CREATE TABLE User(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkACL INT,
            `name` varchar(256),
            username varchar(256),
            `password` varchar(256),
            created_on datetime,
            deleted_on datetime,
            email varchar(256)
        )
        ";
    }

    private function create_buku()
    {
        return "CREATE TABLE Buku(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkPenerbit INT,
            fkTag INT,
            fkUser INT,
            name varchar(256),
            published_on datetime,
            created_on datetime,
            deleted_on datetime
        )
        ";
    }

    private function create_eksemplar()
    {
        return "CREATE TABLE Eksemplar(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkJenis INT,
            fkBuku INT,
            total INT
        )
        ";
    }

    private function create_denda()
    {
        return "CREATE TABLE Denda(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkJenis INT,
            not_before datetime,
            expired_on datetime
        )
        ";
    }

    private function create_dendaRule()
    {
        return "CREATE TABLE DendaRule(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkDenda INT,
            precedence INT,
            min_day INT,
            penalty_price DECIMAL(13, 2)
        )
        ";
    }

    private function create_meminjam()
    {
        return "CREATE TABLE Meminjam(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkEksemplar INT,
            fkUser INT,
            `status` varchar(256),
            due_on datetime,
            returned_on datetime,
            created_on datetime
        )
        ";
    }

    private function create_pesanan()
    {
        return "CREATE TABLE Pesanan(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkEksemplar INT,
            fkUser INT,
            booking_date date,
            total INT,
            created_on datetime
        )
        ";
    }
}
