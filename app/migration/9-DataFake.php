<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class DataFake extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');

        $jenis = [];
        $jenis[] = $db->exec("CALL Insert_Eksemplar_Jenis(:name, @last_insert_id)", [
            ":name" => "Tandon"
        ])[0]['last_inserted_id'];
        $jenis[] = $db->exec("CALL Insert_Eksemplar_Jenis(:name, @last_insert_id)", [
            ":name" => "Cetak"
        ])[0]['last_inserted_id'];
        $jenis[] = $db->exec("CALL Insert_Eksemplar_Jenis(:name, @last_insert_id)", [
            ":name" => "Fotokopi"
        ])[0]['last_inserted_id'];

        // generate buku:
        // -- generate 10 penerbit
        $generator = new \Nubs\RandomNameGenerator\Alliteration();
        $generator2 = new \Nubs\RandomNameGenerator\Vgng();
        $penerbit = [];
        for($i=0; $i<10; $i++) {
            $penerbit[] = $db->exec("CALL Insert_Penerbit(:name, @last_inserted_id)",[
                ":name" => $generator->getName()
            ])[0]['last_inserted_id'];
        }
        
        //generate tags
        $tags = [];
        for($i=0; $i<10; $i++) {
            $tags[] = $db->exec("CALL Insert_Tag(:name, @last_inserted_id)",[
                ":name" => $generator->getName()
            ])[0]['last_inserted_id'];
        }

        // -- generate 10 pengarang
        $pengarang = [];
        for($i=0; $i<10; $i++) {
            $pengarang[] = $db->exec("CALL Insert_Pengarang(:name, @last_inserted_id)",[
                ":name" => $generator->getName()
            ])[0]['last_inserted_id'];
        }
        // -- kasih 30 buku, tiap buku rand(1,3) pengarang, 1 penerbit.
        $bukus = [];
        for($i=0; $i<30; $i++) {
            ($data = [
                ":fkPenerbit" => $penerbit[rand(0,count($penerbit)-1)],
                ":fkPengarang" => $pengarang[rand(0,count($pengarang)-1)],
                ":name" => $generator2->getName(),
                ":published" => date("Y-m-d", strtotime("-" . rand(5,40) . "days")),
                ":tag"=>$tags[rand(0,count($tags)-1)]
            ]);
            //echo "\n\n";
            $row = $db->exec("CALL Insert_Buku(:fkPenerbit, :tag, 1, :fkPengarang, :name, :published, now(), @last_inserted_id)", $data)[0]['last_inserted_id'];
            // add jenis buku

            //add several tags
            for($k=rand(3,5); $k>0; $k--) {
                $db->exec("INSERT INTO TagBuku (fkTag, fkBuku) VALUES (?, ?)", [
                    1=>$tags[rand(0,count($tags)-1)],
                    2=>$row
                ]);
            }

            $eksemplars = [];

            // -- jenis per buku, random 10-20
            foreach($jenis as $j) {
                $db->exec("UPDATE Eksemplar SET total = :total WHERE fkJenis = :fkJenis AND fkBuku = :fkBuku", [
                    ":fkJenis" => $j,
                    ":fkBuku" => $row,
                    ":total" => rand(10,20)
                ]);
                $eksemplars[] = $db->exec("SELECT id from Eksemplar WHERE fkJenis = :fkJenis AND fkBuku = :fkBuku", [
                    ":fkJenis" => $j,
                    ":fkBuku" => $row
                ])[0]['id'];
            }

            // attach pengarang
            for($d=rand(1,4); $d>0; $d--) {
                $db->exec("INSERT BukuPengarang (fkBuku, fkPengarang) VALUES (:fkBuku, :fkPengarang)", [
                    ":fkBuku" => $row,
                    ":fkPengarang" => $pengarang[rand(0, count($pengarang)-1)]
                ]);
            }

            $bukus[] = [
                "book" => $row,
                "eksemplars" => $eksemplars
            ];
        }

        // -- generate 100 user
        $start_month = strtotime("-40 days");
        $end_month = strtotime("-10 days");
        $start_week = strtotime("-7 days");
        $end_week = strtotime("-1 days");
        $user = [];
        var_dump($bukus);
        for($i=100; $i>0; $i--) {
            $uname = strtolower(str_replace(" ", "_", $generator->getName()));
            $user[] = $us = $db->exec("CALL Insert_User(3, :username, :password, :email, @last_inserted_id)", [
                ":username"=> $uname,
                ":password"=>rand(1000,4000) . $uname,
                ":email"=>$uname . "@localhost"
            ])[0]['last_inserted_id'];
            
            // -- tiap user minimal pernah minjem 3 buku, acak (rentang waktu 1 bulan)
            try {
                for($k = 3; $k>0; $k--) {
                    $date = rand($start_month, $end_month);
                    $due = strtotime("+4 days", $date);
                    $return = strtotime("+2 days", $date);
                    $eksem = $bukus[rand(0,count($bukus)-1)]['eksemplars'][0];
                    $data = [
                        ":fkEksem"=>$eksem,
                        ":fkUser"=>$us,
                        ":due"=>date("Y-m-d", $due)
                    ];
                    var_dump($data);
                    echo "\n\n";
                    $r = $db->exec("CALL Insert_Meminjam(:fkEksem, :fkUser, 'returned', :due, now(), @last_insert_id)", $data)[0]['last_inserted_id'];
                    $db->exec("UPDATE Meminjam SET returned_on = :ret WHERE id=:id", [
                        ":ret" => date("Y-m-d", $return),
                        ":id" => $r
                    ]);
                }
            } catch (\Throwable $e) {
                throw new \Exception($e->getMessage(), 0, $e);
            }

            try {
                // -- tiap user minimal minjem 2 buku, acak, dari sini ambil pake eksemplarnya (rentang waktu 1 minggu)
                for($k = 2; $k>0; $k--) {
                    $date = rand($start_week, $end_week);
                    $due = strtotime("+7 days", $date);
                    $eksem = $bukus[rand(0,count($bukus)-1)]['eksemplars'][0];
                    $db->exec("CALL Insert_Meminjam(:fkEksem, :fkUser, 'borrowed', :due, now(), @last_insert_id)", [
                        ":fkEksem"=>$eksem,
                        ":fkUser"=>$us,
                        ":due"=>date("Y-m-d", $due),
                    ]);
                }
            } catch (\Throwable $e) {
                throw new \Exception($e->getMessage(), 0, $e);
            }
        }

        
        $start_week = strtotime("-14 days");
        $end_week = strtotime("-8 days");
        // -- ambil 5-8 user yang sengaja dibikin telat.
        try {
            for($i=rand(5,8); $i>0; $i--) {
                $usernya = $user[rand(0, count($user)-1)];
                $date = rand($start_week, $end_week);
                $due = strtotime("+7 days", $date);
                $eksem = $bukus[rand(0,count($bukus)-1)]['eksemplars'][0];
                $db->exec("CALL Insert_Meminjam(:fkEksem, :fkUser, 'borrowed', :due, now(), @last_insert_id)", [
                    ":fkEksem"=>$eksem,
                    ":fkUser"=>$usernya,
                    ":due"=>date("Y-m-d", $due),
                ]);
            }
        } catch (\Throwable $e) {
            throw new \Exception($e->getMessage(), 0, $e);
        }
    }

    public function on_failed(\Exception $e) {

    }
}
