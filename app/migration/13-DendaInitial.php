<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class DendaInitial extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        
        $jenises = $db->exec("SELECT * FROM EksemplarJenis");
        $mult = 1;
        foreach($jenises as $jenis) {
            $rule = $db->exec("CALL Insert_Denda(:fkJenis, :nbf, :exp, @last_inserted_id)", [
                ":fkJenis" => $jenis['id'],
                ":nbf"=>date("Y-m-d", strtotime("-2 days")),
                ":exp"=>date("Y-m-d", strtotime("+1 year"))
            ])[0]['last_inserted_id'];
            
            $db->exec("CALL Insert_Denda_Rule(:fkDenda, :pre, :min_day, :penal, @last_inserted_id)", [
                ":fkDenda" => $rule,
                ":pre" => 1,
                ":min_day" => 10,
                ":penal" => 5000 * $mult
            ]);
            $db->exec("CALL Insert_Denda_Rule(:fkDenda, :pre, :min_day, :penal, @last_inserted_id)", [
                ":fkDenda" => $rule,
                ":pre" => 2,
                ":min_day" => 5,
                ":penal" => 10000 * $mult
            ]);

            $mult += 0.5;
        }

        $db->exec($this->SP_Denda_Counter());
        $db->exec($this->SP_Denda_Calc());
    }

    private function SP_Denda_Counter(){
        return "
            DROP PROCEDURE IF EXISTS Denda_Counter;
            CREATE PROCEDURE Denda_Counter(
                IN eksemplar_jenis_id INT,
                IN days INT,
                OUT result DECIMAL(13,2))
                proc_Exit:BEGIN
                    DECLARE denda_id INT DEFAULT -1;
                    DECLARE finished INT DEFAULT 0;
                    DECLARE v_min_day INT DEFAULT 0;
                    DECLARE v_penalty_price DECIMAL(13,2) DEFAULT 0;
                    DECLARE temp_days INT DEFAULT 0;
                    DECLARE k INT DEFAULT 0;

                    -- bikin cursor buat denda rule
                    DECLARE cursor_DendaRule CURSOR 
                    FOR
                        SELECT
                            min_day, penalty_price
                        FROM
                            DendaRuleResult;

                    -- fetch status
                    DECLARE CONTINUE HANDLER 
                    FOR NOT FOUND SET finished = 1;

                    CREATE TEMPORARY TABLE DendaRuleResult(
                        min_day INT,
                        penalty_price DECIMAL(13, 2)
                    );

                    SET temp_days = days;

                    SELECT
                        id INTO denda_id
                    FROM
                        Denda
                    WHERE
                        fkJenis = eksemplar_jenis_id AND not_before < NOW() AND expired_on > NOW()
                    ORDER BY
                        id ASC
                    LIMIT 1;

                    INSERT INTO DendaRuleResult
                    SELECT
                        DendaRule.min_day, DendaRule.penalty_price
                    FROM
                        DendaRule
                    WHERE
                        fkDenda = denda_id
                    ORDER BY
                        precedence ASC;
                        
                    SET result = 0;

                    OPEN cursor_DendaRule;

                    FETCH cursor_DendaRule 
                    INTO v_min_day, v_penalty_price;

                        WHILE finished = 0 DO
                            SET k = LEAST(v_min_day,temp_days);
                            SET temp_days = temp_days - v_min_day;
                            SET result = result + k*v_penalty_price;

                            FETCH cursor_DendaRule 
                            INTO v_min_day, v_penalty_price;

                            IF temp_days < 0 THEN
                                SET finished = 1;
                            END IF;

                        END WHILE;

                        -- menangani jika aturan habis dan masih tersisa telat
                        WHILE temp_days > 0 DO
                            SET k = LEAST(v_min_day,temp_days);
                            SET temp_days = temp_days - v_min_day;
                            SET result = result + k*v_penalty_price;
                        END WHILE;
                        
                    CLOSE cursor_DendaRule;
                    
                    DROP TEMPORARY TABLE DendaRuleResult;
                END;
        ";
    }

    private function SP_Denda_Calc(){
        return "DROP FUNCTION IF EXISTS Calculate_Denda;
            CREATE FUNCTION Calculate_Denda(
                fkMeminjam INT)
                RETURNS DECIMAL(13,2)
                BEGIN
                    DECLARE v_id_eksemplar INT;
                    DECLARE v_id_eksemplar_jenis INT;
                    DECLARE v_due_on datetime;
                    DECLARE v_status varchar(256);
                    DECLARE result DECIMAL(13,2);
                    
                    SET result = 0;
                    
                    SELECT fkEksemplar, due_on, status
                        INTO v_id_eksemplar, v_due_on, v_status
                        FROM Meminjam
                        WHERE id = fkMeminjam;
                    
                    IF (v_status = 'borrowed'  AND v_due_on < now()) THEN
                        SELECT fkJenis
                        INTO v_id_eksemplar_jenis
                        FROM Eksemplar
                        WHERE id = v_id_eksemplar;

                        CALL Denda_Counter(v_id_eksemplar_jenis, abs(datediff(v_due_on, now())), @res);

                        RETURN @res;
                        
                    END IF;

                    RETURN result;
                END;
        ";
    }

    public function on_failed(\Exception $e) {

    }
}
