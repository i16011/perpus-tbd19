<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class DBStoredProcedure extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        //if(!$db->exec([
        $db->exec($this->SP_Insert_ACL());
        $db->exec($this->SP_Insert_ACL_Item());
        $db->exec($this->SP_Insert_Buku());
        $db->exec($this->SP_Insert_Denda());
        $db->exec($this->SP_Insert_Denda_Rule());
        $db->exec($this->SP_Insert_Eksemplar());
        $db->exec($this->SP_Insert_Eksemplar_Jenis());
        $db->exec($this->SP_Insert_Meminjam());
        $db->exec($this->SP_Insert_Penerbit());
        $db->exec($this->SP_Insert_Pengarang());
        $db->exec($this->SP_Insert_Pesanan());
        $db->exec($this->SP_Insert_Tag());
        $db->exec($this->SP_Insert_User());
        //])){
        //    throw new \Exception("ERROR!");
        //}
    }

    public function on_failed(\Exception $e) {

    }

    private function SP_Insert_User(){
        return [
            "DROP PROCEDURE IF EXISTS `Insert_User`",
            "CREATE PROCEDURE Insert_User (
                IN fk_acl INT,
                IN username varchar(256),
                IN password varchar(256),
                IN email varchar(256),
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE acl_id INT DEFAULT -1;

                    SELECT
                        id INTO acl_id
                    FROM
                        ACL
                    Where
                        ACL.id = fk_acl;
                    
                    if acl_id = -1 THEN
                        -- error karena tidak ada id ACL
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'ACL id not found';
                        LEAVE proc_Exit;
                    END IF;
                    
                    INSERT INTO User (fkacl, username, password, created_on, deleted_on, email)
                    VALUES (fk_acl, username, password, NOW(), null , email);
                    
                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END"
        ];
    }

    private function SP_Denda_Counter(){
        return "
            DROP PROCEDURE IF EXISTS Denda_Counter;
            CREATE PROCEDURE Denda_Counter(
                IN eksemplar_jenis_id INT,
                IN days INT,
                IN denda_id_param INT,
                OUT result DECIMAL(13,2))
                proc_Exit:BEGIN
                    DECLARE denda_id INT DEFAULT -1;
                    DECLARE finished INT DEFAULT 0;
                    DECLARE min_day INT DEFAULT 0;
                    DECLARE penalty_price DECIMAL(13,2) DEFAULT 0;
                    DECLARE temp_days INT DEFAULT 0;
                    DECLARE k INT DEFAULT 0;

                    -- bikin cursor buat denda rule
                    DECLARE cursor_DendaRule CURSOR 
                    FOR
                        SELECT
                            min_day, penalty_price
                        FROM
                            DendaRuleResult;

                    -- fetch status
                    DECLARE CONTINUE HANDLER 
                    FOR NOT FOUND SET finished = 1;

                    CREATE TEMPORARY TABLE DendaRuleResult(
                        min_day INT,
                        penalty_price DECIMAL(13, 2)
                    );

                    SET temp_days = days;

                    SELECT
                        id INTO denda_id
                    FROM
                        Denda
                    WHERE
                        fkJenis = eksemplar_jenis_id AND not_before < NOW() AND expired_on > NOW()
                    ORDER BY
                        id ASC
                    LIMIT 1;

                    INSERT INTO DendaRuleResult
                    SELECT
                        min_day, penalty_price
                    FROM
                        DendaRule
                    WHERE
                        fk_Denda = denda_id
                    ORDER BY
                        precedence ASC;

                    FETCH cursor_DendaRule 
                    INTO min_day, penalty_price;
                    
                    OPEN cursor_DendaRule;
                        WHILE finished = 0 DO
                            SET k = LEAST(min_day,temp_days);
                            SET temp_days = temp_days - min_day;
                            SET result = result + k*penalty_price;

                            FETCH cursor_DendaRule 
                            INTO min_day, penalty_price;

                        END WHILE;
                    CLOSE cursor_DendaRule;
                    
                    DROP TABLE DendaRuleResult;
                END;
        ";
    }

    private function SP_Insert_Denda(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Denda;
            CREATE PROCEDURE Insert_Denda(
                IN fk_jenis INT,
                IN not_before datetime,
                IN expired_on datetime,
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE denda_id INT DEFAULT -1;
                    DECLARE jenis_id INT DEFAULT -1;

                    SELECT
                        id INTO jenis_id
                    FROM
                        Jenis
                    Where
                        Jenis.id = fk_jenis;

                    if fk_jenis = -1 THEN
                        -- error karena tidak ada fk_jenisnya , sp selesai
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'fk_jenis not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO denda_id
                    FROM
                        Denda
                    WHERE
                        Denda.not_before < not_before AND Denda.expired_on > expired_on AND Denda.fk_jenis = fk_jenis;

                    if denda_id != -1 THEN
                        -- error karena ada sudah ada sebuah denda pada waktu tersebut untuk jenis tersebut , sp selesai
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Sudah ada denda yang berlaku';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Denda (fk_jenis, not_before, expired_on)
                    VALUES (fk_jenis, not_before, expired_on);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Buku(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Buku;
            CREATE PROCEDURE Insert_Buku(
                IN fk_penerbit INT,
                IN fk_tag INT,
                IN fk_user INT,
                IN name varchar(256),
                IN published_on datetime,
                IN created_on datetime,
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE id_penerbit INT DEFAULT -1;
                    DECLARE id_tag INT DEFAULT -1;
                    DECLARE id_user INT DEFAULT -1;

                    SELECT
                        id INTO id_penerbit
                    FROM
                        Penerbit
                    Where
                        Penerbit.id = fk_penerbit;
                    
                    if id_penerbit = -1 THEN
                        -- error karena tidak ada id penerbit
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Penerbit id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_tag
                    FROM
                        Tag
                    Where
                        Tag.id = fk_tag;
                    
                    if id_tag = -1 THEN
                        -- error karena tidak ada id tag
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Tag id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_user
                    FROM
                        User
                    Where
                        User.id = fk_user;
                    
                    if id_user = -1 THEN
                        -- error karena tidak ada id user
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'User id not found';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Buku (fkPenerbit, fkTag, fkUser, name, published_on, created_on, deleted_on)
                    VALUES (fk_penerbit, fk_tag, fk_user, name, published_on, created_on, null);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Denda_Rule(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Denda_Rule;
            CREATE PROCEDURE Insert_Denda_Rule(
                IN fk_denda INT,
                IN precedence INT,
                IN min_day INT,
                IN penalty_price decimal(13,2),
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE id_denda INT DEFAULT -1;
                    DECLARE id_denda_rule INT DEFAULT -1;

                    SELECT
                        id INTO id_denda
                    FROM
                        Denda
                    Where
                        Denda.id = fk_denda;
                    
                    if id_denda = -1 THEN
                        -- error karena tidak ada id denda
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Denda id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_denda_rule
                    FROM
                        Dendarule
                    Where
                        Dendarule.precedence = precedence AND Dendarule.fk_denda = id_denda;
                    
                    if id_denda_rule != -1 THEN
                        -- error karena sudah ada denda rule untuk denda tersebut dengan precedence parameter
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Sudah ada denda rule yang berlaku';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Dendarule (fkDenda, precedence, min_day, penalty_price)
                    VALUES (fk_denda, precedence, min_day, penalty_price);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Eksemplar(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Eksemplar;
            CREATE PROCEDURE Insert_Eksemplar(
                IN fk_jenis INT,
                IN fk_buku INT,
                IN total INT,
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE id_jenis INT DEFAULT -1;
                    DECLARE id_buku INT DEFAULT -1;

                    SELECT
                        id INTO id_jenis
                    FROM
                        EksemplarJenis
                    Where
                        EksemplarJenis.id = fk_jenis;
                    
                    if id_jenis = -1 THEN
                        -- error karena tidak ada id jenis
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Jenis id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_buku
                    FROM
                        Buku
                    Where
                        Buku.id = fk_buku;
                    
                    if id_buku = -1 THEN
                        -- error karena tidak ada id buku
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Buku id not found';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Eksemplar (fkJenis, fkBuku, total)
                    VALUES (fk_jenis, fk_buku, total);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Eksemplar_Jenis(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Eksemplar_Jenis;
            CREATE PROCEDURE Insert_Eksemplar_Jenis(
                IN name varchar(256),
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    INSERT INTO EksemplarJenis (name)
                    VALUES (name);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Meminjam(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Meminjam;
            CREATE PROCEDURE Insert_Meminjam(
                IN fk_eksemplar INT,
                IN fk_user INT,
                IN status varchar(256),
                IN due_on datetime,
                IN returned_on datetime,
                IN created_on datetime,
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE id_eksemplar INT DEFAULT -1;
                    DECLARE id_user INT DEFAULT -1;

                    SELECT
                        id INTO id_eksemplar
                    FROM
                        Eksemplar
                    Where
                        Eksemplar.id = fk_eksemplar;
                    
                    if id_eksemplar = -1 THEN
                        -- error karena tidak ada id eksemplar
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Eksemplar id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_user
                    FROM
                        User
                    Where
                        User.id = fk_user;
                    
                    if id_user = -1 THEN
                        -- error karena tidak ada id user
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'User id not found';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Meminjam (fkEksemplar, fkUser, status, due_on, returned_on, created_on)
                    VALUES (fk_eksemplar, fk_user, status, due_on, returned_on, created_on);
                    
                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Penerbit(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Penerbit;
            CREATE PROCEDURE Insert_Penerbit(
                IN name varchar(256),
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    INSERT INTO Penerbit (name)
                    VALUES (name);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Pengarang(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Pengarang;
            CREATE PROCEDURE Insert_Pengarang(
                IN name varchar(256),
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    INSERT INTO Pengarang (name)
                    VALUES (name);
                    
                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Pesanan(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Pesanan;
            CREATE PROCEDURE Insert_Pesanan(
                IN fk_eksemplar INT,
                IN fk_user INT,
                IN booking_date date,
                IN total INT,
                IN created_on datetime,
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE id_eksemplar INT DEFAULT -1;
                    DECLARE id_user INT DEFAULT -1;

                    SELECT
                        id INTO id_eksemplar
                    FROM
                        Eksemplar
                    Where
                        Eksemplar.id = fk_eksemplar;
                    
                    if id_eksemplar = -1 THEN
                        -- error karena tidak ada id eksemplar
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Eksemplar id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_user
                    FROM
                        User
                    Where
                        User.id = fk_user;
                    
                    if id_user = -1 THEN
                        -- error karena tidak ada id user
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'User id not found';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Pesanan (fkEksemplar, fkUser, booking_date, total, created_on)
                    VALUES (fk_eksemplar, fk_user, booking_date, total, created_on);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Tag(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Tag;
            CREATE PROCEDURE Insert_Tag(
                IN name varchar(256),
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    INSERT INTO Tag (name)
                    VALUES (name);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_ACL(){
        return "
            DROP PROCEDURE IF EXISTS Insert_ACL;
            CREATE PROCEDURE Insert_ACL(
                IN name varchar(256),
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    INSERT INTO ACL (name)
                    VALUES (name);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_ACL_Item(){
        return "
            DROP PROCEDURE IF EXISTS Insert_ACL_Item;
            CREATE PROCEDURE Insert_ACL_Item(
                IN fk_ACL INT,
                IN action_name varchar(256),
                IN value INT,
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE id_ACL INT DEFAULT -1;

                    SELECT
                        id INTO id_ACL
                    FROM
                        ACL
                    Where
                        ACL.id = fk_ACL;
                    
                    if id_ACL = -1 THEN
                        -- error karena tidak ada id ACL
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'ACL id not found';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO ACLItem (fkACL, action_name, value)
                    VALUES (fk_ACL, action_name, value);

                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }
}
