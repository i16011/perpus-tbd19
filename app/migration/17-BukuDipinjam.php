<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class BukuDipinjam extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $db->exec($this->Function_Terpinjam());
    }

    public function on_failed(\Exception $e) {

    }

    private function Function_Terpinjam(){
        return "
        DROP FUNCTION IF EXISTS Terpinjam;
        CREATE FUNCTION Terpinjam (idEksemplar INT)
        RETURNS INT
        BEGIN
            DECLARE terpinjam INT DEFAULT 0;
            
            SELECT
                COUNT(id) INTO terpinjam
            FROM
                Meminjam
            WHERE
                Meminjam.returned_on IS NULL AND Meminjam.fkEksemplar = idEksemplar;
                
            RETURN (terpinjam);
        END
        ";
    }
}
