<?php
namespace Migration;

/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class Many2many extends \Chez14\Ilgar\MigrationPacket
{
    public function on_migrate()
    {
        $f3 = \F3::instance();

        // Tabel blablabla
        $db = $f3->get('DB');
        $db->exec($this->create_bukupengarang());
        $db->exec($this->create_tagbuku());
        $db->exec($this->add_foreign_key());

    }

    public function on_failed(\Exception $e) {

    }


    private function create_bukupengarang()
    {
        return "CREATE TABLE BukuPengarang (
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkPengarang INT,
            fkBuku INT
        )
        ";
    }

    private function create_tagbuku()
    {
        return "CREATE TABLE TagBuku (
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkTag INT,
            fkBuku INT
        )
        ";
    }

    private function add_foreign_key()
    {
        return "
            ALTER TABLE BukuPengarang ADD FOREIGN KEY (fkPengarang) REFERENCES Pengarang(id);
            ALTER TABLE BukuPengarang ADD FOREIGN KEY (fkBuku) REFERENCES Buku(id);
            
            ALTER TABLE TagBuku ADD FOREIGN KEY (fkTag) REFERENCES Tag(id);
            ALTER TABLE TagBuku ADD FOREIGN KEY (fkBuku) REFERENCES Buku(id);
        ";
    }
}
