<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class VSMLanjut extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        
        $db->exec($this->SP_VSMSearch());
    }

    private function SP_VSMSearch(){
        return "
            DROP PROCEDURE IF EXISTS VSM_Search;
            CREATE PROCEDURE VSM_Search(
                IN query varchar(256))
                proc_Exit:BEGIN
                    -- pecah text jadi frekuensi
              
                    -- insert kata-katanya

                    -- VSM: Pisah tan
                    DECLARE a INT DEFAULT 0;
                    DECLARE str VARCHAR(255);
                    DECLARE fullstr VARCHAR(255);
                    DECLARE finished INT DEFAULT 0;
                    DECLARE id_Buku INT;
                    DECLARE jumlah INT;

                    DECLARE cursor_grouper CURSOR
                        FOR
                            SELECT
                                idBuku
                            FROM
                                Grouper
                            Group by
                                idBuku;
                                
                    DECLARE CONTINUE HANDLER 
                        FOR NOT FOUND SET finished = 1;
                    
                    CREATE TEMPORARY TABLE KataIdf(
                        kata  VARCHAR(256),
                        total int DEFAULT 0,
                        idf int DEFAULT 0,
                        bobot decimal(10,3) DEFAULT 0
                    );
                    
                    CREATE TEMPORARY TABLE my_temp_table(
                        kata  VARCHAR(255),
                        total int DEFAULT 0
                    );
                    CREATE TEMPORARY TABLE Relevansi(
                        idBuku  int,
                        distance decimal(10,4)
                    );
                    CREATE TEMPORARY TABLE Grouper(
                        idBuku  int,
                        kata VARCHAR(256),
                        jumlah int,
                        bobot decimal(10,3)
                    );

                    -- =====================================================================================================
                    SET fullstr = query;
                        
                    simple_loop: LOOP
                        SET a=a+1;
                        SET str=SPLIT_STR(fullstr,' ',a);
                        IF str='' THEN
                            LEAVE simple_loop;
                        END IF;
                        -- Do Inserts into temp table here with str going into the row
                        select count(kata) into jumlah from my_temp_table
                            where kata=str;
                        if(jumlah >= 1) THEN
                            UPDATE my_temp_table set total = total + 1 where kata = str;
                        ELSE
                            insert into my_temp_table values (str, 1);
                        end if;

                        END LOOP simple_loop;
           
                    -- insert ke table:
                    insert into KataIdf
                        SELECT my_temp_table.kata as kata,
                            my_temp_table.total as total,
                            _vsm_katas.idf as idf,
                            (1+LOG(2,my_temp_table.total) * _vsm_katas.idf) as bobot
                            FROM my_temp_table inner join _vsm_katas on my_temp_table.kata = _vsm_katas.kata;
                    -- ============================================================================================================================     
                    
                    -- fetch status
                   

                    insert into Grouper
                        SELECT _vsm_buku_kata.fkBuku as idBuku,
                            _vsm_katas.kata as kata,
                            _vsm_buku_kata.jumlah as jumlah,
                            ((1 + LOG(2, _vsm_buku_kata.jumlah)) * _vsm_katas.idf) as bobot
                            FROM _vsm_buku_kata inner join _vsm_katas on _vsm_katas.id = _vsm_buku_kata.fkKatas;
                           

                    OPEN cursor_grouper;
                    
                    FETCH cursor_grouper
                        INTO id_Buku;

                    WHILE finished = 0 DO
                        -- itung A cross B = silang, dot A
                        CREATE TEMPORARY TABLE silence_output select @silang := 0, @dota = 0.0, @dotb=0.0;
                        DROP TEMPORARY TABLE silence_output;
                       
                        CREATE TEMPORARY TABLE silence_output select @silang := sum(Grouper.bobot * KataIdf.bobot) as dot,
                            @dota := sum(Grouper.bobot*Grouper.bobot) as d
                            from
                                Grouper
                            inner join KataIdf on
                                Grouper.kata = KataIdf.kata
                            where idBuku = id_Buku
                            group by idBuku;
                        DROP TEMPORARY TABLE silence_output;

                        -- itung dot B
                        CREATE TEMPORARY TABLE silence_output select @dotb := sum(bobot*bobot)
                        from
                            KataIdf;
                        DROP TEMPORARY TABLE silence_output;

                        insert into Relevansi
                            values(id_Buku, @silang/(sqrt(@dota) * sqrt(@dotb)));
                        
                        FETCH cursor_grouper
                            INTO id_Buku;
                    END WHILE;
                    CLOSE cursor_grouper;

                    -- FINISH
                    select * from Relevansi where distance is not null ORDER BY distance DESC;

                    DROP TABLE my_temp_table;
                    DROP TABLE KataIdf;
                    DROP TABLE Relevansi;
                    DROP TABLE Grouper;
                END;
        ";
    }

    public function on_failed(\Exception $e) {

    }
}
