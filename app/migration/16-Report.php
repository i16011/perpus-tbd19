<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class Report extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $db->exec($this->SP_Report_User());
        $db->exec($this->SP_Report_Buku());
        $db->exec($this->SP_Report_Tag());
        $db->exec($this->SP_Report_All_Tag());
    }

    public function on_failed(\Exception $e) {

    }
    //a
    private function SP_Report_User(){//User paling rajin (user yang paling banyak minjem 30 hari terakhir)
        return "     
            DROP PROCEDURE IF EXISTS List_Pinjaman;
            CREATE PROCEDURE List_Pinjaman()
                proc_Exit:BEGIN
                    SELECT
                        User.id, User.username, COUNT(Meminjam.id) AS jumlah
                    FROM
                        Meminjam
                    INNER JOIN
                        User ON User.id = Meminjam.fkUser
                    WHERE
                        DATEDIFF(Meminjam.created_on,NOW()) <= 30
                    GROUP BY
                        User.id
                    ORDER BY
                        COUNT(Meminjam.id) DESC
                    LIMIT 0,30; -- limit max 30 orang.
                END
        ";
    }
    //b
    private function SP_Report_Buku(){//Buku paling sering di pinjam
        return "
            DROP PROCEDURE IF EXISTS List_Pinjaman_Buku;
            CREATE PROCEDURE List_Pinjaman_Buku()
                proc_Exit:BEGIN
                    SELECT
                        Buku.id, Buku.name, COUNT(Meminjam.id) AS jumlah
                    FROM
                        Meminjam
                    INNER JOIN
                        Eksemplar ON Meminjam.fkEksemplar = Eksemplar.id
                    INNER JOIN
                        Buku ON Eksemplar.fkBuku = Buku.id
                    GROUP BY
                        Buku.id
                    ORDER BY
                        COUNT(Meminjam.id) DESC;
                END
        ";
    }
    //c
    private function SP_Report_Tag(){//tag-tag terpopuler
        return "
            DROP PROCEDURE IF EXISTS List_Pinjaman_Tag;
            CREATE PROCEDURE List_Pinjaman_Tag()
                proc_Exit:BEGIN
                    SELECT
                        Tag.id, Tag.name, COUNT(Meminjam.id) AS jumlah
                    FROM
                        Meminjam
                    INNER JOIN
                        Eksemplar ON Meminjam.fkEksemplar = Eksemplar.id
                    INNER JOIN
                        Buku ON Eksemplar.fkBuku = Buku.id
                    INNER JOIN
                        TagBuku ON TagBuku.fkBuku = Buku.id
                    INNER JOIN
                        Tag ON TagBuku.fkTag = Tag.id
                    GROUP BY
                        Tag.id
                    ORDER BY
                        COUNT(Meminjam.id) DESC;
                END
        ";
    }
    //e
    private function SP_Report_All_Tag(){//tag-tag yang paling sering dipinjam seorang user
        return "
            DROP PROCEDURE IF EXISTS List_Pinjaman_All_Tag;
            CREATE PROCEDURE List_Pinjaman_All_Tag()
                proc_Exit:BEGIN
                    SELECT
                        Tag.id, Tag.name, User.username, COUNT(Meminjam.id) AS jumlah
                    FROM
                        Meminjam
                    INNER JOIN
                        User ON User.id = Meminjam.fkUser
                    INNER JOIN
                        Eksemplar ON Meminjam.fkEksemplar = Eksemplar.id
                    INNER JOIN
                        Buku ON Eksemplar.fkBuku = Buku.id
                    INNER JOIN
                        TagBuku ON TagBuku.fkBuku = Buku.id
                    INNER JOIN
                        Tag ON TagBuku.fkTag = Tag.id
                    WHERE
                        DATEDIFF(Meminjam.created_on,NOW()) <= 30
                    GROUP BY
                        Tag.id, Tag.name, User.username
                    ORDER BY
                        User.username ASC, COUNT(Meminjam.id) DESC;
                END
        ";
    }
}
