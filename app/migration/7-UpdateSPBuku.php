<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class UpdateSPBuku extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $db->exec($this->SP_Insert_Buku());
        $db->exec($this->SP_Insert_Meminjam());
        $db->exec($this->Function_Terpinjam());
        $db->exec($this->SP_Update_Eksemplar());
        $db->exec($this->SP_Insert_Eksemplar_Jenis());
    }

    public function on_failed(\Exception $e) {

    }

    private function SP_Insert_Buku(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Buku;
            CREATE PROCEDURE Insert_Buku(
                IN fk_penerbit INT,
                IN fk_tag INT,
                IN fk_user INT,
                IN fk_pengarang INT,
                IN name varchar(256),
                IN published_on datetime,
                IN created_on datetime,
                OUT last_inserted_id INT)
                proc_Exit: BEGIN
                    DECLARE id_penerbit INT DEFAULT -1;
                    DECLARE id_tag INT DEFAULT -1;
                    DECLARE id_user INT DEFAULT -1;
                    DECLARE id_pengarang INT DEFAULT -1;

                    SELECT
                        id INTO id_penerbit
                    FROM
                        Penerbit
                    Where
                        Penerbit.id = fk_penerbit;
                    
                    if id_penerbit = -1 THEN
                        -- error karena tidak ada id penerbit
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Penerbit id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_pengarang
                    FROM
                        Pengarang
                    Where
                        Pengarang.id = fk_pengarang;
                    
                    if id_pengarang = -1 THEN
                        -- error karena tidak ada id penerbit
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Pengarang id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_tag
                    FROM
                        Tag
                    Where
                        Tag.id = fk_tag;
                    
                    if id_tag = -1 THEN
                        -- error karena tidak ada id tag
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Tag id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_user
                    FROM
                        User
                    Where
                        User.id = fk_user;
                    
                    if id_user = -1 THEN
                        -- error karena tidak ada id user
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'User id not found';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Buku (fkPenerbit, fkTag, fkUser, name, published_on, created_on, deleted_on)
                    VALUES (fk_penerbit, fk_tag, fk_user, name, published_on, created_on, null);

                    SET last_inserted_id = LAST_INSERT_ID();

                    INSERT INTO BukuPengarang (fkPengarang, fkBuku)
                    VALUES (id_pengarang, last_inserted_id);

                    INSERT INTO TagBuku (fkTag, fkBuku)
                    VALUES (id_tag, last_inserted_id);

                    INSERT INTO Eksemplar (fkJenis, fkBuku, total)
                        SELECT EksemplarJenis.id, last_inserted_id, 0 
                        FROM EksemplarJenis;

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function SP_Insert_Meminjam(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Meminjam;
            CREATE PROCEDURE Insert_Meminjam(
                IN fk_eksemplar INT,
                IN fk_user INT,
                IN status varchar(256),
                IN due_on datetime,
                IN created_on datetime,
                OUT last_inserted_id INT)
                proc_Exit:BEGIN
                    DECLARE id_eksemplar INT DEFAULT -1;
                    DECLARE id_user INT DEFAULT -1;

                    SELECT
                        id INTO id_eksemplar
                    FROM
                        Eksemplar
                    Where
                        Eksemplar.id = fk_eksemplar;
                    
                    if id_eksemplar = -1 THEN
                        -- error karena tidak ada id eksemplar
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'Eksemplar id not found';
                        LEAVE proc_Exit;
                    END IF;

                    SELECT
                        id INTO id_user
                    FROM
                        User
                    Where
                        User.id = fk_user;
                    
                    if id_user = -1 THEN
                        -- error karena tidak ada id user
                        SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = 'User id not found';
                        LEAVE proc_Exit;
                    END IF;

                    INSERT INTO Meminjam (fkEksemplar, fkUser, status, due_on, returned_on, created_on)
                    VALUES (fk_eksemplar, fk_user, status, due_on, NULL, created_on);
                    
                    SET last_inserted_id = LAST_INSERT_ID();

                    SELECT last_inserted_id;
                END;
        ";
    }

    private function Function_Terpinjam(){
        return "
        CREATE FUNCTION Terpinjam (id_eksemplar INT)
        RETURNS INT
        BEGIN
            DECLARE total_terpinjam INT;

            SELECT
                COUNT(id) into total_terpinjam
            FROM
                Meminjam
            WHERE
                Meminjam.fkEksemplar = id_eksemplar AND Meminjam.returned_on IS NOT NULL;

            RETURN (total_terpinjam);
        END
        ";
    }

    private function SP_Update_Eksemplar(){
        return "
        DROP PROCEDURE IF EXISTS Update_Eksemplar;
        CREATE PROCEDURE Update_Eksemplar(
            IN id_eksemplar INT,
            IN total INT)
            proc_Exit:BEGIN
                DECLARE total_terpinjam INT;
                
                SET total_terpinjam = Terpinjam(id_eksemplar);

                IF total < total_terpinjam THEN
                    -- error karena seminimal mungkin harus sebanyak yang terpinjam
                    SIGNAL SQLSTATE '45000'
                    SET MESSAGE_TEXT = 'Total tidak sesuai dengan yang terpinjam';
                    LEAVE proc_Exit;
                END IF;

                UPDATE Eksemplar SET Eksemplar.total = total WHERE Eksemplar.id = id_eksemplar;
            END;
        ";
    }

    private function SP_Insert_Eksemplar_Jenis(){
        return "
            DROP PROCEDURE IF EXISTS Insert_Eksemplar_Jenis;
            CREATE PROCEDURE Insert_Eksemplar_Jenis(
                IN name varchar(256),
                OUT last_inserted_id INT)
            proc_Exit:BEGIN
                INSERT INTO EksemplarJenis (name)
                VALUES (name);

                SET last_inserted_id = LAST_INSERT_ID();

                INSERT INTO Eksemplar (fkJenis, fkBuku, total)
                    SELECT last_inserted_id, Buku.id, 0
                    FROM Buku;

                SELECT last_inserted_id;
            END;
        ";
    }
}