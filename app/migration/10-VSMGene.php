<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class VSMGene extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $this->vsm_table_create($db);
        $this->vsm_sp_insert($db);
        $this->vsm_sp_calculate_idf($db);

        // update all benda
        
    }

    public function on_failed(\Exception $e) {

    }

    private function vsm_table_create($db) {
        $db->exec("CREATE TABLE _vsm_buku_kata(
            id INT AUTO_INCREMENT PRIMARY KEY,
            fkBuku int,
            fkKatas int,
            jumlah int
        )
        ");
        $db->exec("CREATE TABLE _vsm_katas(
            id INT AUTO_INCREMENT PRIMARY KEY,
            kata varchar(256),
            idf decimal(8,4)
        )
        ");

        // ALTER
        $db->exec("ALTER TABLE _vsm_buku_kata ADD FOREIGN KEY (fkBuku) REFERENCES Buku(id);");
        $db->exec("ALTER TABLE _vsm_buku_kata ADD FOREIGN KEY (fkKatas) REFERENCES _vsm_katas(id);");
    }

    private function vsm_sp_insert($db) {
        $db->exec("DROP PROCEDURE IF EXISTS vsm_update_words_book;
        CREATE
            PROCEDURE vsm_update_words_book( IN fkBuku INT,
                IN title varchar(256) )
            BEGIN
                DECLARE judul varchar(256) DEFAULT NULL;

            DECLARE v_kata varchar(256) DEFAULT NULL;

            DECLARE fullstr varchar(256) DEFAULT NULL;

            DECLARE v_jumlah INT DEFAULT NULL;

            DECLARE kata_vsm_katas varchar(256) DEFAULT NULL;

            DECLARE id_vsm_katas INT DEFAULT NULL;

            DECLARE id_vsm_buku_kata INT DEFAULT NULL;

            DECLARE finished INT DEFAULT 0;

            DECLARE a INT Default 0 ;

            DECLARE str VARCHAR(255);

            DECLARE cursor_KataRes CURSOR FOR SELECT
                kata,
                jumlah
            FROM
                KataRes;
            -- fetch status
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET
            finished = 1;

            CREATE
                TEMPORARY TABLE
                    my_temp_table( kata VARCHAR(255),
                    total int DEFAULT 0 );

            CREATE
                TEMPORARY TABLE
                    KataRes( kata varchar(256) PRIMARY KEY,
                    jumlah INT );
                
            CREATE
                TEMPORARY TABLE KataTable(
                    fkBuku int,
                    fkKatas int,
                    jumlah INT
                );
            -- ambil title bukunya dulu, kalo nggak ada, baru ambil ke tabel buku
            SET
            judul = title;

            IF judul IS NULL THEN SELECT
                name into
                    judul
                FROM
                    Buku
                WHERE
                    id = fkBuku;
            END IF;

            SET fullstr = judul;
            -- ========================================================================================
            simple_loop: LOOP 
                SET a = a + 1;
                
                SET
                str = SPLIT_STR(fullstr, ' ', a);
                
                IF str = '' THEN 
                    LEAVE simple_loop;
                END IF;
                
                #Do Inserts into temp table here with str going into the row
                SELECT
                    @jumlah := count(kata)
                from
                    my_temp_table
                where
                    kata = str;
                
                IF(@jumlah >= 1) THEN
                    UPDATE my_temp_table set
                        total = total + 1
                    where
                        kata = str;
                ELSE 
                    INSERT into my_temp_table
                    values (str, 1);
                end if;
            END LOOP simple_loop;
            -- =================================================================================

            insert into KataRes select * from my_temp_table;

            select * from KataRes;

            OPEN cursor_KataRes;

            FETCH cursor_KataRes INTO
                v_kata,
                v_jumlah;

            -- bersihin semua kata pada buku
            DELETE FROM _vsm_buku_kata
                WHERE _vsm_buku_kata.fkBuku = fkBuku;

                -- untuk tiap kata, update, ato insert kalo nggak ada.
            WHILE finished = 0 DO
                -- cari id dari kata tersebut
                SELECT
                        id INTO
                            id_vsm_katas
                        FROM
                            _vsm_katas
                        WHERE
                            _vsm_katas.kata = v_kata;
                
                -- jika belum ada di insert
                IF (id_vsm_katas IS NULL) THEN 
                    INSERT INTO _vsm_katas (kata, idf)
                        VALUES (v_kata, null);
                
                    SET id_vsm_katas = LAST_INSERT_ID();
                END IF;

                INSERT INTO _vsm_buku_kata (fkBuku, fkKatas, jumlah)
                    VALUES (fkBuku, id_vsm_katas, v_jumlah);
                
            --     INSERT INTO KataTable (fkBuku, fkKatas, jumlah)
            --     	VALUES (fkBuku, id_vsm_katas, v_jumlah);

                -- UPDATRE IDF masing masing kata
                UPDATE _vsm_katas
                    SET
                        idf = vsm_calculate_idf(id_vsm_katas)
                    WHERE
                        _vsm_katas.id = id_vsm_katas;
                    
                FETCH cursor_KataRes INTO
                    v_kata,
                    v_jumlah;
            END WHILE;

            select * from KataTable;

            CLOSE cursor_KataRes;
            -- drop temporary table
            DROP
                TABLE
                    KataRes;

            drop
                table
                    my_temp_table;

            drop
                table
                    KataTable;
            END");
    }

    private function vsm_sp_calculate_idf($db) {
        $db->exec("
            DROP FUNCTION IF EXISTS vsm_calculate_idf;
            CREATE FUNCTION vsm_calculate_idf(
                idKata INT
            )
            RETURNS decimal(8,4)
            BEGIN
                DECLARE jumlah_dokumen INT DEFAULT NULL;
                DECLARE jumlah_dokumen_dengan_kata INT DEFAULT NULL;

                SELECT
                    COUNT(id) into jumlah_dokumen
                FROM
                    Buku;

                SELECT
                    COUNT(id) into jumlah_dokumen_dengan_kata
                FROM
                    _vsm_buku_kata
                WHERE
                    _vsm_buku_kata.fkKatas = idKata;
                
                RETURN LOG(2, CAST(jumlah_dokumen as DECIMAL)/CAST(jumlah_dokumen_dengan_kata as DECIMAL));
            END
        ");
    }
}
