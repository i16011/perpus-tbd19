<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class DBInitData extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        $f3 = \F3::instance();
        
        $db = $f3->get('DB');
        $db->exec("INSERT INTO ACL(`name`) VALUES (?)", ['Superuser']);
        $db->exec("INSERT INTO ACL(`name`) VALUES (?)", ['Admin']);
        $db->exec("INSERT INTO ACL(`name`) VALUES (?)", ['Petugas']);
        $db->exec("INSERT INTO ACL(`name`) VALUES (?)", ['Member']);
        $acl = $db->lastInsertId();

        $db->exec('INSERT INTO ACLItem(`fkACL`, `action_name`, `value`) VALUES(?, ?, ?)', [1=>1, 2=>'base',3=>15]);
        $db->exec('INSERT INTO ACLItem(`fkACL`, `action_name`, `value`) VALUES(?, ?, ?)', [1=>2, 2=>'base',3=>15]);
        $db->exec('INSERT INTO ACLItem(`fkACL`, `action_name`, `value`) VALUES(?, ?, ?)', [1=>3, 2=>'base',3=>7]);
        $db->exec('INSERT INTO ACLItem(`fkACL`, `action_name`, `value`) VALUES(?, ?, ?)', [1=>4, 2=>'base',3=>2]);
    }

    public function on_failed(\Exception $e) {

    }
}
