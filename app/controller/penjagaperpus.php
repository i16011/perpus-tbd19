<?php
namespace Controller;

class Penjagaperpus
{

    private $colors = [
        "#f44336",
        "#e91e63",
        "#9c27b0",
        "#673ab7",
        "#3f51b5",
        "#00bcd4",
        "#03a9f4",
        "#009688",
        "#4caf50",
        "#8bc34a",
        "#cddc39",
        "#ffeb3b",
        "#ffc107",
        "#ff9800",
        "#ff5722",
        "#795548",
        "#9e9e9e",
        "#607d8b"
    ];

    public function get_index($f3)
    {
        $f3->reroute("penjagaperpus/book");
    }

    public function get_book($f3)
    {
        $books = [];
        if($f3->exists("GET.q") && $f3->GET['q']) {
            $search = $f3->DB->exec("CALL VSM_Search(:query)", [
                ":query" => $f3->GET['q']
            ]);
            if(!!$search && count($search) > 0) {
                $main_query = "SELECT Buku.*, Penerbit.name as pener FROM Buku INNER JOIN Penerbit on Buku.fkPenerbit = Penerbit.id WHERE deleted_on is null AND (";
                $query = [];
                $key = 1;
                foreach($search as $idbuku) {
                    $main_query .= " Buku.id = ? OR";
                    $query[$key++] = $idbuku['idBuku'];
                }
                $main_query = substr($main_query, 0, -2) . ")";
                $books = $f3->DB->exec($main_query, $query);
            }
        } else {
            $books = $f3->DB->exec("SELECT Buku.*, Penerbit.name as pener FROM Buku INNER JOIN Penerbit on Buku.fkPenerbit = Penerbit.id WHERE deleted_on is null ORDER BY Buku.id DESC");
        }
        $f3->set('data.books', $books);

        // TAGS
        $tags = $f3->DB->exec("SELECT TagBuku.fkBuku as id, Tag.name as name, Tag.id as tag_id FROM TagBuku INNER JOIN Tag on TagBuku.fkTag = Tag.id ORDER BY TagBuku.id");
        $tag = [];
        foreach ($tags as $b) {
            $tag[$b['id']][] = $b;
        }
        $f3->set('data.tag', $tag);

        $tags = $f3->DB->exec("SELECT * FROM Tag");
        $f3->set('data.tags', $tags);

        // PENGARANG
        $pengarangs = $f3->DB->exec("SELECT BukuPengarang.fkBuku as id, Pengarang.name as name from BukuPengarang inner join Pengarang on BukuPengarang.fkPengarang = Pengarang.id");
        $penga = [];
        foreach ($pengarangs as $b) {
            $penga[$b['id']][] = $b;
        }
        $f3->set('data.pengarang', $penga);

        $pengarang = $f3->DB->exec("SELECT * from Pengarang");
        $f3->set('data.pengarangs', $pengarang);

        // PENERBIT
        $penerbit = $f3->DB->exec("SELECT * FROM Penerbit");
        $f3->set('data.penerbits', $penerbit);

        // TIPE
        $tipe = $f3->DB->exec("SELECT * FROM EksemplarJenis");
        $f3->set('data.tipe', $tipe);

        //eksemplar jenis:
        $eksemplar = $f3->DB->exec("SELECT Eksemplar.fkBuku as id, 
            Eksemplar.id as eksem_id, 
            EksemplarJenis.name as name, 
            Eksemplar.total as total,
            Stock_Buku(Eksemplar.id) as dapat_dipinjam
            FROM Eksemplar inner join EksemplarJenis on Eksemplar.fkJenis = EksemplarJenis.id");
        $semplar = [];
        foreach ($eksemplar as $b) {
            $semplar[$b['id']][] = $b;
        }
        $f3->set("data.eksemplar", $semplar);

        // SEARCH CODE BAKAL DI SINI

        return \View\Template::render('penjaga-perpus/book.html', 'Book');
    }

    public function delete_book($f3)
    {
        header("Content-type: application/json");
        $f3->DB->exec("UPDATE Buku set deleted_on = now() where id = ?", [
            1 => $f3->GET['id']
        ]);
        echo json_encode(["status" => "ok"]);
    }

    public function post_book($f3)
    {
        //do some checkups here
        // - id
        // - other fields
        if ($f3->exists("POST.id")) {
            $book = $f3->get('DB')->exec("SELECT id FROM Buku where id=?", [
                1 => $f3->get('POST.id')
            ]);
            if (!$book) {
                \Flash::instance()->addMessage("Cannot update nonexistent book.", "danger");
                return $this->get_book($f3);
            }

            $f3->DB->exec("UPDATE Buku SET fkPenerbit = ?, `name` = ? WHERE id=?", [
                1 => $f3->POST['penerbits'],
                2 => $f3->POST['name'],
                3 => $f3->POST['id']
            ]);

            \Flash::instance()->addMessage("Update Successfull for book:" . $book['id'], "success");
            return $this->get_book($f3);
        }
        $res = [];
        try {
            $pengarangs = $f3->POST['pengarangs'];
            $tags = $f3->POST['tags'];

            $res = $f3->DB->exec("CALL Insert_Buku(:fkPenerbit, :fkTag, :fkUser, :fkPengarang, :name, :published, now(), @last_inserted_id)", [
                ":fkPenerbit" => $f3->POST['penerbits'],
                ":fkUser" => $f3->USER['id'],
                ":name" => $f3->POST['name'],
                ":published_on" => $f3->POST['published_on'],
                ":fkPengarang" => array_pop($pengarangs),
                ":fkTag" => array_pop($tags)
            ])[0]['last_inserted_id'];
            while($p = array_pop($pengarangs)) {
                $f3->DB->exec("INSERT INTO BukuPengarang (fkPengarang, fkBuku) VALUES(?, ?)", [
                    1=>$p,
                    2=>$res
                ]);
            }
            while($p = array_pop($tags)) {
                $f3->DB->exec("INSERT INTO TagBuku (fkTag, fkBuku) VALUES(?, ?)", [
                    1=>$p,
                    2=>$res
                ]);
            }
            
        } catch (\Exception $e) {
            \Flash::instance()->addMessage("Error occured when trying to create a book: " . $e->getMessage(), "danger");
            return $this->get_book($f3);
        }

        \Flash::instance()->addMessage("Insert Successfull for book:" . $res[0]['last_inserted_id'], "success");
        return $this->get_book($f3);
    }

    public function get_member($f3)
    {
        $members = [];
        if($f3->exists("GET.q") && $f3->get("GET.q")) {
            $members = $f3->DB->exec("SELECT *
                FROM User
                WHERE User.deleted_on is null AND (User.username like ? OR User.name like ?) ORDER BY User.id", [
                    1=>"%" . $f3->GET['q'] . "%",
                    2=>"%" . $f3->GET['q'] . "%",
                ]);
        } else {
            $members = $f3->DB->exec("SELECT *
                FROM User
                WHERE User.deleted_on is null ORDER BY User.id");

        }
        $f3->set('data.members', $members);

        return \View\Template::render('penjaga-perpus/member.html', 'Member');
    }

    public function get_borrowing($f3)
    {
        $borrows = $f3->DB->exec("SELECT User.username,
            Meminjam.id as id,
            Buku.name AS nama_buku, 
            Meminjam.due_on,
            datediff(Meminjam.due_on, now()) as beda,
            (Calculate_Denda(Meminjam.id)) as denda
            FROM Meminjam 
            INNER JOIN Eksemplar ON Meminjam.fkEksemplar = Eksemplar.id 
            INNER JOIN Buku ON Eksemplar.fkBuku = Buku.id
            INNER JOIN User ON User.id = Meminjam.fkUser
            WHERE Buku.deleted_on is null AND  Meminjam.returned_on is null
            ORDER BY beda asc, Buku.id asc");
        $f3->set('data.borrows', $borrows);
        // get all all authors,
        // get all tags of current user.
        return \View\Template::render('penjaga-perpus/borrowing.html', 'Borrowing');
    }

    public function post_borrowing($f3) {
        $borrows = $f3->DB->exec("SELECT *, Calculate_Denda(Meminjam.id) as denda FROM Meminjam WHERE id =?", [1=>$f3->POST['id']]);
        if (!$borrows) {
            \Flash::instance()->addMessage("Cannot update nonexistent borrowing history.", "danger");
            return $this->get_borrowing($f3);
        }
        $f3->DB->exec("UPDATE Meminjam SET `status` = ?, returned_on = now() WHERE id = ?", [1=>$f3->POST['status'], 2=>$f3->POST['id']]);
        \Flash::instance()->addMessage("Successfully updated borrowing status for meminjam_id = " . $f3->POST['id'] . ", eksemplar = " . $borrows[0]['fkEksemplar'] . ". With penalty as much as Rp. " . $borrows[0]['denda'] . ",- .", "success");
        return $this->get_borrowing($f3);
    }

    public function get_report($f3)
    {
        // REPORT A ========================================================================================================
        $f3->set("data.user_most_borrow", $f3->DB->exec("CALL List_Pinjaman()"));

        // REPORT B ========================================================================================================
        $f3->set("data.buku_most_hit", $f3->DB->exec("CALL List_Pinjaman_Buku()"));

        // REPORT C ========================================================================================================
        $tags = $f3->DB->exec("CALL List_Pinjaman_Tag()");
        $dataset = [
            "datasets"=>[[
                "data"=>[],
                "backgroundColor"=>[],
                "label" => "Tags Percentage"
            ]],
            "labels" => []
        ];
        foreach($tags as $t) {
            $dataset['datasets'][0]['data'][] = intval($t['jumlah']);
            $dataset['datasets'][0]['backgroundColor'][] = $this->colors[rand(0,count($this->colors)-1)];
            $dataset['labels'][] = $t['name'];
        }
        $f3->set("data.tag_terbanyak_dipinjam", $dataset);
        $f3->set("data.tag_terbanyak_dipinjam_raw", $tags);

        // REPORT E ========================================================================================================
        $hasil = $f3->DB->exec("CALL List_Pinjaman_All_Tag()");
        $user = [];
        foreach($hasil as $h) {
            if(!array_key_exists($h['username'], $user)) {
                $user[$h['username']] = [
                    "username" => $h['username'],
                    "tags" => []
                ];
            }

            $user[$h['username']]['tags'][] = [
                "name" => $h['name'],
                "jumlah" => $h['jumlah']
            ];
        }
        // prepare dataset
        $users = array_map(function($data){
            // dataset
            $dataset = [
                "datasets"=>[[
                    "data"=>[],
                    "backgroundColor"=>[],
                    "label"=>"Tags Distribution"
                ]],
                "labels" => []
            ];
            foreach($data['tags'] as $t) {
                $dataset['datasets'][0]['data'][] = $t['jumlah'];
                $dataset['datasets'][0]['backgroundColor'][] = $this->colors[rand(0,count($this->colors)-1)];
                $dataset['labels'][] = $t['name'];
            }
            return array_merge($data, [
                "data"=>$dataset
            ]);
        },array_values($user));

        $f3->set("data.user_tags", $users);

        return \View\Template::render('penjaga-perpus/report.html', 'Report');
    }
}
