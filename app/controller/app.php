<?php
Namespace Controller;

class App extends \Prefab {
    public function get_hello($f3){
        if(!$f3->USER) {
            return $f3->reroute("/user/login");
        }
        
        if(!$f3->USER['ACL'] || $f3->USER['ACL']['name'] == "Member") {
            return $f3->reroute("/pengunjung");
        }

        return $f3->reroute("/penjagaperpus");
    }
}