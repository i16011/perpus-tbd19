<?php
namespace Controller;

class User
{
    public function get_login($f3)
    {
        return \View\Template::render('user/login.html', 'Login');
    }

    public function post_login($f3)
    {
        // check for implementation and things
        if (!$f3->exists("POST.username") || !$f3->exists("POST.password") || empty($f3->get("POST.username")) || empty($f3->get("POST.password"))) {
            \Flash::instance()->addMessage('Username dan Password harus ada', "danger");
            return $this->get_login($f3);
        }

        $user = $f3->DB->exec("SELECT * from User where (email=? or username=?) AND (deleted_on is null)", [
            1 => $f3->POST['username'],
            2 => $f3->POST['username']
        ]);

        if (count($user) == 0 || !\Model\User::verify_password($user[0]['password'], $f3->POST['password'])) {
            \Flash::instance()->addMessage('Kombinasi username dan password tidak ditemukan.', 'danger');
            return $this->get_login($f3);
        }

        $user = $user[0];

        //get ACL info
        $acl = $f3->DB->exec("SELECT * FROM ACL where id=?", [
            1 => $user['fkACL']
        ]);

        // if it's all good, then save it to the session.
        $f3->set('SESSION.user', array_merge($user, [
            "ACL" => $acl[0]
        ]));
        // prepare for mock rotator
        $f3->USER = $f3->SESSION['user'];
        return \Controller\App::instance()->get_hello($f3);
    }

    public function get_logout($f3)
    {
        $f3->clear('SESSION');
        \Flash::instance()->addMessage('Logout berhasil. Terima kasih sudah berkunjung.', "info");
        return $f3->reroute("/user/login");
    }
}
