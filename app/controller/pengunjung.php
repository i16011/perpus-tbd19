<?php
namespace Controller;

class Pengunjung
{
    public function get_index($f3)
    {
        $books = [];
        if($f3->exists("GET.q") && $f3->GET['q']) {
            $search = $f3->DB->exec("CALL VSM_Search(:query)", [
                ":query" => $f3->GET['q']
            ]);
            if(!!$search && count($search) > 0) {
                $main_query = "SELECT Buku.*, Penerbit.name as pener FROM Buku INNER JOIN Penerbit on Buku.fkPenerbit = Penerbit.id WHERE deleted_on is null AND (";
                $query = [];
                $key = 1;
                foreach($search as $idbuku) {
                    $main_query .= " Buku.id = ? OR";
                    $query[$key++] = $idbuku['idBuku'];
                }
                $main_query = substr($main_query, 0, -2) . ")";
                $books = $f3->DB->exec($main_query, $query);
            }
        } else {
            $books = $f3->DB->exec("SELECT Buku.*, Penerbit.name as pener FROM Buku INNER JOIN Penerbit on Buku.fkPenerbit = Penerbit.id WHERE deleted_on is null ORDER BY Buku.id DESC");
        }
        
        $f3->set('data.books', $books);
        
        // TAGS
        $tags = $f3->DB->exec("SELECT TagBuku.fkBuku as id, Tag.name as name, Tag.id as tag_id FROM TagBuku INNER JOIN Tag on TagBuku.fkTag = Tag.id ORDER BY TagBuku.id");
        $tag = [];
        foreach($tags as $b) {
            $tag[$b['id']][] = $b;
        }
        $f3->set('data.tag', $tag);

        $tags = $f3->DB->exec("SELECT * FROM Tag");
        $f3->set('data.tags', $tags);

        // PENGARANG
        $pengarangs = $f3->DB->exec("SELECT BukuPengarang.fkBuku as id, Pengarang.name as name from BukuPengarang inner join Pengarang on BukuPengarang.fkPengarang = Pengarang.id");
        $penga = [];
        foreach($pengarangs as $b) {
            $penga[$b['id']][] = $b;
        }
        $f3->set('data.pengarang', $penga);

        $pengarang = $f3->DB->exec("SELECT * from Pengarang");
        $f3->set('data.pengarangs', $pengarang);

        // PENERBIT
        $penerbit = $f3->DB->exec("SELECT * FROM Penerbit");
        $f3->set('data.penerbits', $penerbit);

        // get all all authors,
        // get all tags of current user.
        
        return \View\Template::render('user/home.html', 'Home');
    }

    public function get_rent($f3)
    {
        // belum dimasukkin fkUser yang lagi loginnya, denda belum dimasukkin
        $books = $f3->DB->exec("SELECT Buku.name, Meminjam.due_on, (Calculate_Denda(Meminjam.id)) as denda
            FROM Meminjam 
            INNER JOIN Eksemplar ON Meminjam.fkEksemplar = Eksemplar.id 
            INNER JOIN Buku ON Eksemplar.fkBuku = Buku.id
            WHERE Meminjam.fkUser = ? AND Buku.deleted_on is null ORDER BY Buku.id", [
            1=>$f3->USER['id']
        ]);
        $f3->set('data.books', $books);
        
        // TAGS
        $tags = $f3->DB->exec("SELECT TagBuku.fkBuku as id, Tag.name as name, Tag.id as tag_id FROM TagBuku INNER JOIN Tag on TagBuku.fkTag = Tag.id ORDER BY TagBuku.id");
        $tag = [];
        foreach($tags as $b) {
            $tag[$b['id']][] = $b;
        }
        $f3->set('data.tag', $tag);

        $tags = $f3->DB->exec("SELECT * FROM Tag");
        $f3->set('data.tags', $tags);


        return \View\Template::render('user/rent.html', 'Rent');
    }

    public function get_edit_profile($f3)
    {
        return \View\Template::render('user/editprofile.html', 'Edit profile');
    }
}
