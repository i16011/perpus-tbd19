<?php
namespace Model;

class User {
    public static function hash_password($pass) {
        return password_hash($pass, CRYPT_BLOWFISH);
    }

    public static function verify_password($hash, $password) {
        return password_verify($password, $hash);
    }
}